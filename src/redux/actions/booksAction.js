import {ADD_FAV, DELETE_FAV } from "../actionTypes/actionTypes";

const addItemFavorite = (book) => {
  return (dispatch) => {
    let books = JSON.parse(localStorage.getItem("books")) || [];
    const existingIndex = books.findIndex(i => i.id === book.id);
    if (existingIndex === -1) {
      books.push(book);
      localStorage.setItem("books", JSON.stringify(books));
    }
    dispatch({ type: ADD_FAV, payload: books });
  }
};

const deleteItemFavorite = (id) => {
  return (dispatch) => {
    const storedBooks = JSON.parse(localStorage.getItem("books")) || [];
    const updatedBooks = storedBooks.filter((item) => item.id !== id);
    localStorage.setItem("books", JSON.stringify(updatedBooks));
    dispatch({ type: DELETE_FAV, payload: updatedBooks });
  };
};

export { addItemFavorite, deleteItemFavorite };
