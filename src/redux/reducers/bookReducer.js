import { ADD_FAV, DELETE_FAV } from "../actionTypes/actionTypes";

const initialState = {
  payload: JSON.parse(localStorage.getItem("books")) || [],
};

export const bookReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_FAV:
      return {
        ...state,
        payload: action.payload,
      };

    case DELETE_FAV:
      return {
        ...state,
        payload: action.payload,
      };
    default:
      return state;
  }
};