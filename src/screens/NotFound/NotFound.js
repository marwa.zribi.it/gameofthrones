import React  from 'react';
import './NotFound.css'
const NotFound = () => {
   return (
    <div className={"container-not-found"}>
      <div className={"error"}>
        Erreur 404
      </div>
      <div className={"details"}>
       La page que vous cherchez est non trouvée
      </div>
    </div>
  );
};

export default NotFound;
