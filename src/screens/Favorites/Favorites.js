import React  from 'react';
import './favorites.css'
import NavigationBar from "../../components/NavigationBar/NavigationBar";
import {useDispatch, useSelector} from "react-redux";
import {deleteItemFavorite} from "../../redux/actions/booksAction";
const Favorites = () => {
  const payload = useSelector(state => state.payload);
  const dispatch = useDispatch();
  const removeFromFav = (item) => {
    dispatch(deleteItemFavorite(item.isbn));
  }
  return (
    <div className={"container-fav"}>
        <NavigationBar />
      <div className={"data-container-fav"}>
        <div className={"error"}>
          Les Favoris
        </div>
        <div className={"favorite-list-container"}>
          { payload!== undefined?  payload.map((item, i) => {
             return(
              <div className={"item-fav"} key={i}>
                <div>{`${i + 1} - ${item.name} by ${item.authors}`}</div>
                <div className={'button-in-list'} onClick={() => removeFromFav(item)}>Supprimer</div>
              </div>

            )
          }):null}
        </div>
      </div>


    </div>
  );
};

export default Favorites;
