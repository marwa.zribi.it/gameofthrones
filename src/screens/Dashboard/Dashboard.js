import React, { useState, useEffect } from 'react';
import "./dashboard.css"
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import axios from 'axios';
import Modal from "../../components/Modal/Modal";
import {addItemFavorite, deleteItemFavorite} from "../../redux/actions/booksAction";
import NavigationBar from "../../components/NavigationBar/NavigationBar";

const Dashboard = () => {
  const [data, setData] = useState();
  const [dataCharacters, setDataCharacters] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingCharacters, setIsLoadingCharacters] = useState(false);
  const [page, setPage] = useState(1);
  const [selectedItem, setSelectedItem] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      const result = await axios.get('https://anapioficeandfire.com/api/books');
      setData(result.data);
      setIsLoading(false);
    };
    if (isLoading) {
      fetchData();
    }
  }, [isLoading]);

  const [isModalOpen, setModalOpen] = useState(false);


  const handleModalOpen = (item) => {
    setModalOpen(true);
    setSelectedItem(item);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  const dispatch = useDispatch();
  const payload = useSelector(state => state.payload);
   const handleAddToFavorite = (item) => {
     dispatch(addItemFavorite({ name: item.name, id :item.isbn, authors:item.authors.join(" ,") }));
  };
  const handleDeleteFromFav = (item) => {
    dispatch(deleteItemFavorite(item.isbn));
  }
  const getCharacters = ()=>{
    setPage(page + 50)
    setIsLoadingCharacters(true)
  }

  useEffect(() => {
    const fetchCharacters = async (page) => {
      setIsLoadingCharacters(true);
      const result = await axios.get(`https://www.anapioficeandfire.com/api/characters?page=${page}&pageSize=50`);
      const res = result.data

      let charactersOfSelectedBook = res.filter(r=>(r.books.filter(m=>m === selectedItem.url)).length !== 0)
      if(charactersOfSelectedBook.length === 0){
        setPage(page + 50)
      }
      console.log('charactersOfSelectedBook',charactersOfSelectedBook)
      setDataCharacters(result.data);
      setIsLoadingCharacters(false);
    };
    if (isLoadingCharacters) {
      fetchCharacters();
    }
  }, [isLoadingCharacters]);



  return (
    <div className={"container"}>
      <NavigationBar />
      <div>
        {isLoading ?
          <div className={"button-list"}>
          Loading ...
        </div> : null}
        {data ? (
          <div className={"data-container"}>
            {data.map((item, i) => {
              const itemIsInFav = payload.find(pay => pay.id === item.isbn);
              return(
                <div className={"item"} key={i}>
                  <div className={'book-name'}>{`${i + 1} - ${item.name}`}</div>
                  <div className={"div-buttons-container"}>
                    <div className={'button-in-list'} onClick={() => {
                      handleModalOpen(item)
                    }}>Détails
                    </div>
                    {itemIsInFav? <div className={'button-in-list'} onClick={() => handleDeleteFromFav(item)}>Supprimer des Favoris
                    </div>:  <div className={'button-in-list'} onClick={() => handleAddToFavorite(item)}>Ajouter aux Favoris
                    </div>}

                  </div>
                </div>

              )
            })}
            </div>
        ) :
          <div className={"button-list"} onClick={() =>setIsLoading(true)}>
          Lister les livres
        </div>
        }
        {isModalOpen ?
          <Modal closeModal={handleModalClose} isOpen={isModalOpen}>
          <div className={"title-details"}>{selectedItem.name}</div>
          <div className={"content-details"}>
            <div className={'details-left-content'}>
              <div className={"title-of-details"}>Auteur(s) :</div>
              <div className={"title-of-details"}>Pays de réalisation :</div>
              <div className={"title-of-details"}>Nombre de pages :</div>
              <div className={"title-of-details"}>Entrprise d'edition :</div>
              <div className={"title-of-details"}>Date de sortie :</div>
              <div className={"title-of-details"}>Personnages :</div>
            </div>
            <div className={'details-right-content'}>
              <div className={"details-text"}>{selectedItem.authors.join(' ,')}</div>
              <div className={"details-text"}>{selectedItem.country}</div>
              <div className={"details-text"}>{selectedItem.numberOfPages}</div>
              <div className={"details-text"}>{selectedItem.publisher}</div>
              <div className={"details-text"}>{(new Date(selectedItem.released)).toDateString()}</div>
              <div className={"details-text"}>{selectedItem.characters.length} <div className={"link-to-explore"} onClick={()=>{getCharacters()}}>Explorer →</div></div>
              {dataCharacters ? (
                  <div className={"characters-container"}>
                    {dataCharacters.map((item, i) => {
                      return(
                          <div key={"char"+i} className={'characters-text'}>{`${i + 1} ${item.aliases.join(" , ")} ${item.name !== undefined && item.name.length !==0 ?`-Nom :  ${item.name}` :""} `}</div>
                      )
                    })}
                  </div>
                ) :
               null
              }
            </div>
          </div>
        </Modal>
          :null}

      </div>

    </div>
  );
};






export default React.memo(Dashboard);
