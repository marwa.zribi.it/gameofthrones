import React  from 'react';
import "./modal.css"
const Modal = ({ isOpen, closeModal, children}) => {
  return (
    <div className={`modal ${isOpen ? 'modal-visible' : ''}`}>
      <div className="modal-content">
        <button onClick={closeModal} className="modal-close-button">
          X
        </button>
        {children}
      </div>
    </div>
  );
};

export default Modal;
