import React from 'react';

const BackgroundWrapper = ({ children }) => {
  return (
    <div style={{
      backgroundImage: `url('/background/GOT.jpg')`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      minHeight: '100vh'
    }}>
      {children}
    </div>
  );
};

export default BackgroundWrapper;
