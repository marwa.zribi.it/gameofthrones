import React from "react";
import { Link } from "react-router-dom";
import './navigationBar.css'
const NavigationBar = () => (
  <nav className={'nav-bar'}>

        <Link to="/"><div className={"text-nav"}>Home</div></Link>
        <Link to="/fav"><div className={"text-nav"}>Favorite</div></Link>

  </nav>
);

export default NavigationBar;