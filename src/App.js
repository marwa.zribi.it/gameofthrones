import './App.css';
import {Provider} from "react-redux";
import Dashboard from "./screens/Dashboard/Dashboard";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import store from "./redux/store/store";
import NotFound from "./screens/NotFound/NotFound";
import BackgroundWrapper from "./components/BackgroundWrapper";
import Favorites from "./screens/Favorites/Favorites";

function App() {
  return (
    <Provider store={store}>
      <BackgroundWrapper>
        <Router>
          <Routes>
            <Route exact path='/' element={(<Dashboard/>)} />
            <Route exact path='/fav' element={(<Favorites/>)} />
            <Route path="*" element={(<NotFound/>)} />
          </Routes>
        </Router>
      </BackgroundWrapper>

    </Provider>
  );
}

export default App;
