import axios from "axios";

export async function getData(url) {
  try {
    const response = await axios.get(url);
    const data = response.data;
    console.log(data);
  } catch (error) {
    console.error(error);
  }
}

export async function postData(url, body) {
  try {
    const response = await axios.post(url, body);
    console.log(response.data);
  } catch (error) {
    console.error(error);
  }
}